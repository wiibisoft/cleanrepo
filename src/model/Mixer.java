package model;

import java.awt.Color;
import java.awt.geom.Point2D;

import view.Kettle;
import view.Potion;
import view.RewardPotion;

public class Mixer {

	private int red = 0, green = 0, blue = 0, tolerantie = 5,easterId=-1;
	private boolean easter = false;
	private double[] colors = new double[4]; // 0 red ; 1 geen ; 2 blue ; 4 number
											// of drups
	private ScoreSystem scoreSystem;
	private EasterList easterList = new EasterList();
	private Color color=new Color(0,0,0,0);
	private Kettle kettle;
	private Color emptyKettle = (new Color(43, 100, 255,155));
	
	public Mixer() {
	}

	public void setKettle(Kettle kettle){
		this.kettle = kettle;
	}
	/**
	 * @ param Potion newone ; the potion that has been added
	 * 
	 * @ param int value ; the number of added drops
	 */
	public void addPotion(Potion newone, double value) {
		colors[0] += newone.getRed()*value;
		colors[1] += newone.getGreen()*value;
		colors[2] += newone.getBlue()*value;
		colors[3] += value;
		color = (new Color((int) (colors[0] / colors[3]),
				(int) (colors[1] / colors[3]), (int) (colors[2] / colors[3]),155));
		kettle.setColor(color);
	}
	
	public void setScoreSystem(ScoreSystem scoreSystem){
		this.scoreSystem = scoreSystem;
	}

	public double[] getColors() {
		return colors;
	}
	
	public Color getColor(){
		return color;
	}

	/*
	 * @ return ; the total number of drops in the kettle
	 */
	public double howFull() {
		return colors[3];
	}

	/*
	 * the formule on how to calculate
	 */
	public void calculateColor() {
		if (colors[3]!=0){
		this.red = (int) (colors[0] / colors[3]);
		this.green = (int) (colors[1] / colors[3]);
		this.blue = (int) (colors[2] / colors[3]);
		}
		else {
			this.red=0;
			this.green=0;
			this.blue=0;
		}
		if (red == 0 && green == 0 && blue == 0)
			color = emptyKettle;
		else
			color = (new Color(red, green, blue,155));
	}


	/*
	 * the mixing check + point setter
	 */
	public Potion mixIt()
	{
		int checkRed = 0;
		int checkGreen = 0;
		int checkBlue = 0;
		boolean found = false;

		calculateColor();
		Point2D.Double point = new Point2D.Double(500, 500);

		for (int idx = 0; idx < easterList.numberOfEasters(); idx++)
		{
			if (!found)
			{
				easterList.newEaster(idx);
				checkRed = easterList.getRed();
				checkGreen = easterList.getgreen();
				checkBlue = easterList.getblue();

				if (red >= checkRed - tolerantie
						&& red <= checkRed + tolerantie)
				{
					if (green >= checkGreen - tolerantie
							&& green <= checkGreen + tolerantie)
					{
						if (blue >= checkBlue - tolerantie
								&& blue <= checkBlue + tolerantie)
						{
							easter = true;
							easterId = easterList.getEaster();
							scoreSystem.incrementScore(easterList.getPoints(),
									point);
							easterList.removeEaster(idx);
							found = true;
						}
					}
				}
			}
		}

		if (easter) {
			// play the easterId easter
		}
		else 
			scoreSystem.incrementScore(red + green + blue, point);
		int tempred=red;
		int tempgreen=green;
		int tempblue=blue;
		empty();
		return new RewardPotion(100, 0, "rewardPotion" + tempred + tempgreen + tempblue,
				tempred, tempgreen, tempblue);
	}
	
	public void empty(){
		for (int i = 0; i < 4; i++) {
			colors[i] = 0;
		}
		color = emptyKettle;
	}
	
	public int getEaster(){
		if (easter){
			easter=false;
			return easterId;
		}
		return -1;
	}
}
