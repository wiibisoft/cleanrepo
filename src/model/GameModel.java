package model;
 
import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Timer;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

import wiiusej.values.Orientation;
import wiiusej.values.RawAcceleration;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import view.DisplayWindow;
import view.EasterPlay;
import view.Floor;
import view.GameScreen;
import view.Hand;
import view.InfoScreen;
import view.Kettle;
import view.Potion;
import view.Screen;
import view.Shelf;
import view.WorldObject;

public class GameModel implements ActionListener
{
	private DisplayWindow view;
	private ScreenManager screenManager = new ScreenManager();
	private Physics physics = null;
	
	private Timer gameTimer = null;
	
	private int irPosXWiimote1;
	private int irPosYWiimote1;
	
	private int irPosXWiimote2;
	private int irPosYWiimote2;
	
	private boolean lose = false;
	private float angleWiimote1;
	private float angleWiimote2;
	private ScoreSystem scoreSystem;
	private Mixer mixer = new Mixer();

	private List<Potion> createdPotions;
	private List<WorldObject> worldObjects;
	private RawAcceleration accelerationWiimote1;
	private RawAcceleration accelerationWiimote2;
	private int potionLimit = 5;
	
	private long time = System.currentTimeMillis();
	private MusicPlayer musicPlayer;
	private Thread backgroundMusicThread;
	
	public GameModel(DisplayWindow view)
	{
		createdPotions = new ArrayList<Potion>();
		worldObjects = new LinkedList<WorldObject>();
		this.view = view;
		
		scoreSystem = new ScoreSystem(300, screenManager);
		physics = new Physics();
		
		mixer.setScoreSystem(scoreSystem);
		screenManager.initialize(view,mixer);
		musicPlayer = new MusicPlayer();
		backgroundMusicThread = new Thread(musicPlayer);
		backgroundMusicThread.start();
		
		gameTimer = new Timer(1000 / 60, this);
		gameTimer.start();
	}
	
	public void testEffects()
	{
		Screen currentScreen = screenManager.getCurrentScreen();
		Random rand = new Random();
		scoreSystem.incrementScore(200, new Point2D.Double(rand.nextInt(1280), rand.nextInt(720)));
		((GameScreen) currentScreen).setKettleColor(new Color(rand.nextInt(256),rand.nextInt(256),rand.nextInt(256)));
	}

	public void updateIRPosition(int x, int y, int wiimoteId)
	{
		if(wiimoteId == 1)
		{
			irPosXWiimote1 = x;
			irPosYWiimote1 = y;
			
		}
		else
		{
			irPosXWiimote2 = x;
			irPosYWiimote2 = y;
		}
	}
	
	public Point calculateIRPosition(int wiiId)
	{
		// Hier alle berekeningen doen voordat je het gaat doorpassen naar de view.
		if(wiiId == 1)
		{
			Point finalPosition = new Point(irPosXWiimote1,irPosYWiimote1);
			return finalPosition;
		}
		else
		{
			Point finalPosition = new Point(irPosXWiimote2,irPosYWiimote2);
			return finalPosition;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
			
		// TODO Auto-generated method stub		
		screenManager.updateMotionInput(calculateMotionInput(1), 1);
		screenManager.updateMotionInput(calculateMotionInput(2), 2);
		screenManager.updateIRPosition(calculateIRPosition(1).x, calculateIRPosition(1).y, 1);
		screenManager.updateIRPosition(calculateIRPosition(2).x, calculateIRPosition(2).y, 2);
		
		if (accelerationWiimote1 != null && accelerationWiimote2 != null)
		{
			if (screenManager.getCurrentScreen() instanceof GameScreen)
			{
				if (((GameScreen) screenManager.getCurrentScreen()).getKettle().checkShake(accelerationWiimote1.getX(),	accelerationWiimote2.getX()))
				{
					((GameScreen) screenManager.getCurrentScreen()).setObjectReleased(1);
					((GameScreen) screenManager.getCurrentScreen()).setObjectReleased(2);
					Potion potion = mixer.mixIt();
					((GameScreen) screenManager.getCurrentScreen()).addPotion(potion);
					createPhysicsObject(potion);
					((GameScreen) screenManager.getCurrentScreen()).getKettle().setColor(mixer.getColor());
					addPotion(potion);
				}
			}
		}
		lose();
		collision();

		int easter = mixer.getEaster(); // als dit -1 = dan niks doen ander easter van de easter ID afspelen dus die variabele moet op een andere plek staan
		if (screenManager.getCurrentScreen() instanceof GameScreen)
		{	
			if (easter!=-1){
				((GameScreen) screenManager.getCurrentScreen()).addEaster(new EasterPlay(easter));
			}	
		}
		if (screenManager.getCurrentScreen() instanceof GameScreen)
		{	
			((GameScreen) screenManager.getCurrentScreen()).updateEaster();
		}
	}
	
	public void updateMotionInput(Orientation orientation, int wiimoteId)
	{
		if(wiimoteId == 1)
		{
			angleWiimote1 = orientation.getRoll();
		}
		else
		{
			angleWiimote2 = orientation.getRoll();
		}
	}

	public double calculateMotionInput(int wiiId)
	{
		if(wiiId == 1)
		{
			double finalAngle = Math.toRadians(angleWiimote1);
			return finalAngle;	
		}
		else
		{
			double finalAngle = Math.toRadians(angleWiimote2);
			return finalAngle;	
		}	
	}

	public void setScreen(int screenID)
	{
		// TODO Auto-generated method stub
		screenManager.setScreen(screenID);
	}

	public void handleGrabInput(int wiimoteId)
	{
		// TODO Auto-generated method stub
		screenManager.setObjectGrabbed(wiimoteId);
	}

	public void handleGrabReleasedInput(int wiimoteId)
	{
		// TODO Auto-generated method stub
		screenManager.setObjectReleased(wiimoteId);
	}

	public Screen getScreen()
	{
		// TODO Auto-generated method stub
		return screenManager.getCurrentScreen();
	}
	
	public void addPhysicsObjects(List<WorldObject> worldObjects){
		this.worldObjects.clear();
		this.worldObjects.addAll(worldObjects);
		createPhysicsObjects();
	}
	
	private void createPhysicsObjects(){
		for (WorldObject object : worldObjects) {
			if(object instanceof Potion){
				object.addCollisionModel(50, 65, 20, false);
				object.getCollisionModel().setPosition(object.getxPos(), object.getyPos());
				physics.addWorldObject(object);
			}
			else if(object instanceof Shelf){
				object.addCollisionModel(450, 10, 0, true);
				object.getCollisionModel().setPosition(object.getxPos(), object.getyPos());
				physics.addWorldObject(object);
			}
			else if(object instanceof Floor){
				object.addCollisionModel(2500, 10, 0, true);
				object.getCollisionModel().setPosition(object.getxPos(), object.getyPos());
				physics.addWorldObject(object);
			}
		}
	}
	
	private void createPhysicsObject(WorldObject object){
		if (object instanceof Potion) {
			object.addCollisionModel(50, 65, 20, false);
			object.getCollisionModel().setPosition(object.getxPos(),
					object.getyPos());
			physics.addWorldObject(object);
		} else if (object instanceof Shelf) {
			object.addCollisionModel(450, 10, 0, true);
			object.getCollisionModel().setPosition(object.getxPos(),
					object.getyPos());
			physics.addWorldObject(object);
		} else if (object instanceof Floor) {
			object.addCollisionModel(2500, 10, 0, true);
			object.getCollisionModel().setPosition(object.getxPos(),
					object.getyPos());
			physics.addWorldObject(object);
		}
	}

	public void updateRawAcceleration(RawAcceleration acceleration,	int wiimoteId)
	{
		// TODO Auto-generated method stub
		if(wiimoteId == 1)
		{
			accelerationWiimote1 = acceleration;
		}
		else
		{
			accelerationWiimote2 = acceleration;
		}
	}
	
	public void addPotion(Potion potion)
	{
		if(createdPotions.size() < potionLimit )
		{
			createdPotions.add(potion);
			if(createdPotions.size() == potionLimit)
			{
				playSound("res/music/You-Win-Sound-Effect.wav");
				screenManager.setScreen(3);
			}
		}
	}
	
	public void lose(){
		if(scoreSystem.getScore() <= 0){
			screenManager.setScreen(3);
			if(!lose){
				playSound("res/music/game-over.wav");
				lose = true;
			}
		}
	}
		
	public void collision(){
		if(screenManager.getCurrentScreen() instanceof GameScreen){
			GameScreen screen = (GameScreen) screenManager.getCurrentScreen();
			screen.setPotionLockL(false);
			screen.setPotionLockR(false);
			worldObjects = screen.getWorldObjects();
		}
		
		Potion p1 = null;
		Potion p2 = null;
		LinkedList<Potion> potions = new LinkedList<Potion>();
		
		for (WorldObject object : worldObjects) {
			if(object instanceof Potion){
				if(!((Potion) object).getCollisionModel().getGravityEffected()){
					if(p1 == null){
						p1 = (Potion) object;
					}
					else{
						p2 = (Potion) object;
					}
				}
				else{
					potions.add((Potion) object);
				}
			}
		}
		
		if(!(p1 == null && p2 == null)){
			if(p2 == null){
				for(Potion potion: potions){
					if(p1.getShape().intersects((Rectangle2D) potion.getShape())){
						playSound("res/glasses-hit.wav");
					}
				}
			} 
			else if(p1.getShape().intersects((Rectangle2D) p2.getShape())){
				if (screenManager.getCurrentScreen() instanceof GameScreen){
					((GameScreen) screenManager.getCurrentScreen()).setPotionLockL(true);
					((GameScreen) screenManager.getCurrentScreen()).setPotionLockR(true);
				}
				playSound("res/Glass Breaking-SoundBible.com-1765179538.wav");
				p1.breaking();
				p2.breaking();
				if (System.currentTimeMillis() - time > 800) {
					time = System.currentTimeMillis();
					p1.setDraw(false);
					p2.setDraw(false);
				}
			}
			else{
				for(Potion potion: potions){
					if(p1.getShape().intersects((Rectangle2D) potion.getShape())){
						playSound("res/glasses-hit.wav");
					}
					else if(p2.getShape().intersects((Rectangle2D) potion.getShape())){
						playSound("res/glasses-hit.wav");
					}
				}
			}
		}
		
		for (Potion potion: potions) {
			if(!potion.getDraw()){
				worldObjects.remove(potion);
			}
		}
		
		if(screenManager.getCurrentScreen() instanceof GameScreen){
			GameScreen screen = ((GameScreen) screenManager.getCurrentScreen());
			screen.setLeftHandOccupied(false);
			screen.setRightHandOccupied(false);
		}
	}
	
	public void setScore(int score){
		scoreSystem.setScore(score);
	}
	
	public void resetScoreDecrementTimer(){
		scoreSystem.resetTimeCounter();
	}

	private void playSound(String filename) {
		// ** add this into your application code as appropriate
		// Open an input stream to the audio file.
		InputStream in = null;
		try {
			in = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Create an AudioStream object from the input stream.
		AudioStream as = null;
		try {
			as = new AudioStream(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Use the static class member "player" from class AudioPlayer to play
		// clip.
		if (System.currentTimeMillis() - time > 1000) {
			time = System.currentTimeMillis();
			AudioPlayer.player.start(as);
		}
	}
	
	public void resetMixer(){
		createdPotions.clear();
		this.mixer = new Mixer();
		this.mixer.setScoreSystem(scoreSystem);
	}
	
	public void resetGameScreen(){
		
		screenManager.resetGameScreen(this.mixer);
	}
	
	public Screen getSpecificScreen(int index){
		return screenManager.getSpecificScreen(index);
	}
	
	public void resetPhysics(){
		this.physics = new Physics();
	}
	
	public void resetScoreSystem(){
		lose = false;
		scoreSystem.setScore(300);
		
		//get gameScreen to set its score.
		GameScreen gameScreen  = (GameScreen)getSpecificScreen(2); 		
		gameScreen.setScore(scoreSystem.getScore());
	}

	public boolean continueInfoScreen() {
		// TODO Auto-generated method stub
		if(screenManager.getCurrentScreen() instanceof InfoScreen)
		{
			return ((InfoScreen) screenManager.getCurrentScreen()).continueScreen();
		}
		else{
			return false;
		}
	}
}
