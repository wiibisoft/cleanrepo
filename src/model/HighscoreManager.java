package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class HighscoreManager
{
	public static HighscoreList loadHighscores()
	{
		ObjectInputStream input = null;
		try
		{
			input = new ObjectInputStream(new FileInputStream("highscores"));
			try
			{
				HighscoreList highscoreList = (HighscoreList) input.readObject();
				input.close();
				return highscoreList;
			}
			catch (ClassNotFoundException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		catch (IOException e1)
		{
			HighscoreList defaultHighscores = new HighscoreList();
			defaultHighscores.addHighscore(150, "DIC", "BUT");
			defaultHighscores.addHighscore(600, "JER", "RY1");
			defaultHighscores.addHighscore(300, "HAR", "RY6");
			defaultHighscores.addHighscore(100, "WIL", "LEM");
			saveHighscores(defaultHighscores);
			return defaultHighscores;

		}
		return null;
	}

	public static void saveHighscores(HighscoreList highscores)
	{
		ObjectOutputStream output;
		try
		{
			output = new ObjectOutputStream(new FileOutputStream("highscores"));
			output.writeObject(highscores);
			output.close();

		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();

		}
	}
}
