package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HighscoreList implements Serializable
{
	private ArrayList<Highscore> highscores;
	
	public HighscoreList()
	{
		highscores = new ArrayList<Highscore>();
	}
	//adds highscore, and also removes the highscores that are not
	//in the top 10.
	public void addHighscore(int highscore, String playerName1, String playerName2)
	{
		sortHighscores();
		if(highscores.size() < 11){
			highscores.add(new Highscore(highscore, playerName1, playerName2));
			sortHighscores();
		}
		while(highscores.size() >10){
			highscores.remove(10);
		}
		
		
	}
	
	public void removeHighscore(int index)
	{
		highscores.remove(index);
	}
	
	public ArrayList<Highscore> getHighscores()
	{
		sortHighscores();
		return highscores;
	}
	
	public void sortHighscores()
	{
		Collections.sort(highscores, new Comparator<Highscore>()
		{
		    public int compare(Highscore highscore1, Highscore highscore2) 
		    {
		        return highscore1.getHighscore() > highscore2.getHighscore() ? -1 : highscore1.getHighscore() == highscore2.getHighscore() ? 0 : 1;
		    }
		});
	}
}
