package model;

import java.io.File;

import javax.sound.sampled.*;

public class MusicPlayer implements Runnable{
	private File soundFile = null;
	private AudioInputStream soundInput = null;
	
	
	public MusicPlayer() {
		
		// Create an InputStream object.
		try{
			soundFile = new File("res/music/music-combined.wav");
			soundInput = AudioSystem.getAudioInputStream(soundFile);
			// Open an audio input stream.
		} catch(Exception e){
			System.out.println("Music file not found.");
		}
	}
	
	/** create a musicplayer for another file then the background music **/
	public MusicPlayer(String filePath) {
		
		// Create an InputStream object.
		try{
			soundFile = new File(filePath);
			soundInput = AudioSystem.getAudioInputStream(soundFile);
			// Open an audio input stream.
		} catch(Exception e){
			e.printStackTrace();
			System.out.println("Music file not found.");
		}
	}

	@Override
	public void run() {
		try{
		Clip clip = AudioSystem.getClip(); 
		clip.open(soundInput);
		clip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
