package model;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;

import javax.swing.Timer;

import view.GameScreen;
import view.PointsPopup;
import view.ScoreScreen;

public class ScoreSystem implements ActionListener {
	private int totalScore = 300;
	private int timeCounter = 0;
	private int decrementSeverity = 1;
	private int timeBorder = 150;
	private Timer timer = new Timer(1000/10, this);
	
	private ScreenManager screenManager = null;
	
	/**
	 * @param startingScore
	 * startingScore wordt meegegeven om dit als een waarde te houden die eventueel
	 * in de toekomst gewijzigd kan worden, handig voor testen. 
	 */
	public ScoreSystem(int startingScore, ScreenManager screenManager){
		this.screenManager = screenManager;
		totalScore = startingScore;
		timer.start();
	}
	
	/**
	 * Deze klasse checkt of boolean active true is.
	 * Als dit niet het geval is zal puntenaftrek op score toegepast worden.
	 */
	public void checkScoreDecremention(){
		
			if(timeCounter > timeBorder){
				totalScore -= decrementSeverity;
		}
	}
	
	
	
	/**
	 * @param score
	 * Geef een score mee, de huidige score wordt hiermee verhoogt
	 */
	public void incrementScore(int score, Point2D popupPosition){
		
		totalScore += score;
		if(screenManager.getCurrentScreen() instanceof GameScreen){
			GameScreen gameScreen = (GameScreen) screenManager.getCurrentScreen();
			
			//make a fade object for the given score. 
			gameScreen.addFadeObject(new PointsPopup((int) popupPosition.getX(), (int)popupPosition.getY(), 5, Color.YELLOW, score));
			resetTimeCounter();
		}		
		
	}
	
	
	/**
	 * @return
	 * returned een score die geprint kan worden in view.
	 */
	public int getScore(){
		return totalScore;
	}

	/**
	 * Reset functie voor timeCounter variabele.
	 * Kettle zal deze aanroepen als een potion aangemaakt is.
	 */
	public void resetTimeCounter(){
		timeCounter = 0;
	}
	
	public void setScore(int score){
		totalScore = score;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		
		if(screenManager.getCurrentScreen() instanceof GameScreen){
			timeCounter++;
			checkScoreDecremention();
			GameScreen gameScreen = (GameScreen) screenManager.getCurrentScreen();
			gameScreen.setScore(totalScore);
		}
		
		if(screenManager.getCurrentScreen() instanceof ScoreScreen){
			ScoreScreen scoreScreen = (ScoreScreen) screenManager.getCurrentScreen();
			scoreScreen.setScore(totalScore);
		}
	}
	
	
}
