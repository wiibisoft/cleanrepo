package model;

import java.util.ArrayList;

public class EasterList {
	int[] easter = new int[5];
	private ArrayList<int[]> easters=new ArrayList<int[]>();
	
	public EasterList(){
		fillList();
	}
	
	public void fillList(){
		int[] easter = new int[5];
		easters.clear();

		// black
		easter[0]=2; //red
		easter[1]=2; //green
		easter[2]=2; //blue
		easter[3]=0; //eastercount
		easter[4]=-1000;// points for this easter
		easters.add(easter);

		//dark sea green
		easter[0]=143; //red
		easter[1]=188; //green
		easter[2]=143; //blue
		easter[3]=0; //eastercount
		easter[4]=-800;// points for this easter
		easters.add(easter);

		//firebrick
		easter[0]=178; //red
		easter[1]=34; //green
		easter[2]=34; //blue
		easter[3]=0; //eastercount
		easter[4]=-700;// points for this easter
		easters.add(easter);
		
		//aquamarijn
		easter[0]=118; //red
		easter[1]=238; //green
		easter[2]=198; //blue
		easter[3]=0; //eastercount
		easter[4]=500;// points for this easter
		easters.add(easter);
		
		//geelbruin
		easter[0]=205; //red
		easter[1]=133; //green
		easter[2]=63; //blue
		easter[3]=1; //eastercount
		easter[4]=1500;// points for this easter
		easters.add(easter);

		//corn sienna
		easter[0]=139; //red
		easter[1]=69; //green
		easter[2]=19; //blue
		easter[3]=1; //eastercount brown
		easter[4]=1200;// points for this easter
		easters.add(easter);
		
		//magenta
		easter[0]=255; //red
		easter[1]=0; //green
		easter[2]=255; //blue
		easter[3]=2; //eastercount purple
		easter[4]=900;// points for this easter
		easters.add(easter);

		// dark orchid
		easter[0]=153; //red
		easter[1]=50; //green
		easter[2]=204; //blue
		easter[3]=2; //eastercount purple
		easter[4]=1100;// points for this easter
		easters.add(easter);
		
		//crimson
		easter[0]=220; //red
		easter[1]=20; //green
		easter[2]=60; //blue
		easter[3]=3; //eastercount red
		easter[4]=100;// points for this easter
		easters.add(easter);

		//indian red
		easter[0]=205; //red
		easter[1]=92; //green
		easter[2]=92; //blue
		easter[3]=3; //eastercount red
		easter[4]=950;// points for this easter
		easters.add(easter);

		//olive
		easter[0]=128; //red
		easter[1]=128; //green
		easter[2]=0; //blue
		easter[3]=4; //eastercount green
		easter[4]=500;// points for this easter
		easters.add(easter);

		// green yellow
		easter[0]=173; //red
		easter[1]=255; //green
		easter[2]=47; //blue
		easter[3]=4; //eastercount green
		easter[4]=1000;// points for this easter
		easters.add(easter);

		//teal
		easter[0]=0; //red
		easter[1]=128; //green
		easter[2]=128; //blue
		easter[3]=5; //eastercount blue
		easter[4]=1250;// points for this easter
		easters.add(easter);

		//corn flower blue
		easter[0]=100; //red
		easter[1]=149; //green
		easter[2]=237; //blue
		easter[3]=5; //eastercount blue
		easter[4]=20;// points for this easter
		easters.add(easter);
	}
	
	public void removeEaster(int idx){
		easters.remove(idx);
	}
	
	public void newEaster(int index){
		easter=easters.get(index);
	}

	public int getRed(){
		return easter[0];
	}
	public int getgreen(){
		return easter[1];
	}
	public int getblue(){
		return easter[2];
	}

	public int getEaster(){
		return easter[3];
	}
	
	public int getPoints(){
		return easter[4];
	}
	
	public int numberOfEasters(){
		return easters.size();
	}
}
