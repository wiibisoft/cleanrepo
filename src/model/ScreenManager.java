package model;

import java.util.ArrayList;

import controller.KeyboardController;
import view.DisplayWindow;
import view.GameScreen;
import view.InfoScreen;
import view.IntroScreen;
import view.ScoreScreen;
import view.Screen;
import view.StartScreen;

public class ScreenManager
{
	 private ArrayList<Screen> screens = new ArrayList<Screen>();
     private Screen currentScreen = new Screen(99);
	 private DisplayWindow view;
     
     public ScreenManager()
     {

     }
     
     public void initialize(DisplayWindow view, Mixer mixer){
    	 this.view = view;
    	 screens.add(new StartScreen());
    	 screens.add(new InfoScreen());
    	 screens.add(new GameScreen(mixer));
    	 screens.add(new ScoreScreen());
    	 screens.add(new IntroScreen());
    	 setScreen(0);
     }
     
     public void setScreen(int index)
     {
         currentScreen = screens.get(index);
         view.setContentPane(currentScreen);
     }

	public void addKeyListener(KeyboardController keyboardController)
	{
		// TODO Auto-generated method stub
		view.addKeyListener(keyboardController);
	}
	
	public Screen getCurrentScreen()
	{
		return currentScreen;
	}

	public void updateIRPosition(int x, int y, int wiiId)
	{
		// TODO Auto-generated method stub
			if(currentScreen instanceof GameScreen)
			{
				((GameScreen) currentScreen).setHandPosition(x, y, wiiId);
			}
	}

	public void updateMotionInput(double angle, int wiiId)
	{
		// TODO Auto-generated method stub
		if(currentScreen instanceof GameScreen)
		{
			((GameScreen) currentScreen).setHandAngle(angle, wiiId);
		}
	}
	

	public void setObjectGrabbed(int wiiId)
	{
		// TODO Auto-generated method stub
		if(currentScreen instanceof GameScreen)
		{
			((GameScreen) currentScreen).setObjectGrabbed(wiiId);
		}
	}
	
	public void setObjectReleased(int wiiId)
	{
		if(currentScreen instanceof GameScreen)
		{
			((GameScreen) currentScreen).setObjectReleased(wiiId);
		}
	}
	
	public void resetGameScreen(Mixer mixer){
		screens.set(2, new GameScreen(mixer));
	}
	
	public Screen getSpecificScreen(int index){
		return screens.get(index);
	}
}
