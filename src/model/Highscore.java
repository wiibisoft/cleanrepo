package model;

import java.io.Serializable;

public class Highscore implements Serializable
{
	private int highscore = 0;
	private String name1 = "";
	private String name2 = "";
	
	public Highscore(int highscore, String name1, String name2)
	{
		this.highscore = highscore;
		this.name1 = name1;
		this.name2 = name2;
	}

	public int getHighscore()
	{
		return highscore;
	}

	public void setHighscore(int highscore)
	{
		this.highscore = highscore;
	}

	public String getName1()
	{
		return name1;
	}

	public void setName1(String name1)
	{
		this.name1 = name1;
	}

	public String getName2()
	{
		return name2;
	}

	public void setName2(String name2)
	{
		this.name2 = name2;
	}
	
}
