package main;

import model.GameModel;
import controller.MainController;
import view.DisplayWindow;

public class Main
{
	public static void main(String[] args)
    {
        DisplayWindow view = new DisplayWindow();
        GameModel model = new GameModel(view);
        MainController controller = new MainController(view, model);
    }
}