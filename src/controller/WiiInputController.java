package controller;

import wiiusej.wiiusejevents.physicalevents.ExpansionEvent;
import wiiusej.wiiusejevents.physicalevents.IREvent;
import wiiusej.wiiusejevents.physicalevents.MotionSensingEvent;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;
import wiiusej.wiiusejevents.utils.WiimoteListener;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.DisconnectionEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.StatusEvent;

public class WiiInputController implements WiimoteListener
{
        private InputController inputController;

        public WiiInputController(InputController inputController)
        {
                this.inputController = inputController;

        }

        @Override
        public void onButtonsEvent(WiimoteButtonsEvent wiiInput)
        {
                inputController.handleButtonInput(wiiInput, wiiInput.getWiimoteId());
        }


        @Override
        public void onClassicControllerInsertedEvent(
                        ClassicControllerInsertedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onClassicControllerRemovedEvent(
                        ClassicControllerRemovedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onDisconnectionEvent(DisconnectionEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onExpansionEvent(ExpansionEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onGuitarHeroInsertedEvent(GuitarHeroInsertedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onGuitarHeroRemovedEvent(GuitarHeroRemovedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onIrEvent(IREvent irInput)
        {
                // TODO Auto-generated method stub
        	inputController.handleIRInput(irInput, irInput.getWiimoteId());
        }

        @Override
        public void onMotionSensingEvent(MotionSensingEvent motionInput)
        {
                // TODO Auto-generated method stub
        		inputController.handleMotionInput(motionInput.getOrientation(), motionInput.getWiimoteId());
        		inputController.handleAccelerationInput(motionInput.getRawAcceleration(), motionInput.getWiimoteId());
        }

        @Override
        public void onNunchukInsertedEvent(NunchukInsertedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onNunchukRemovedEvent(NunchukRemovedEvent arg0)
        {
                // TODO Auto-generated method stub

        }

        @Override
        public void onStatusEvent(StatusEvent arg0)
        {
                // TODO Auto-generated method stub

        }

}