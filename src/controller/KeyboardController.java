package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import view.InfoScreen;
import view.ScoreScreen;
import view.StartScreen;


public class KeyboardController implements KeyListener 
{

	private InputController inputController;

	public KeyboardController(InputController inputController)
	{
		// TODO Auto-generated constructor stub
		this.inputController = inputController;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("Testing");
		if(arg0.getKeyCode() == KeyEvent.VK_SPACE)
		{
			if(inputController.getScreen() instanceof InfoScreen){
				inputController.changeScreen(2);
				inputController.getMainController().setScore(300);
				inputController.getMainController().resetScoreDecrementTimer();
			}
			
			if(inputController.getScreen() instanceof StartScreen){
				inputController.changeScreen(1);
			}
			
			if(inputController.getScreen() instanceof ScoreScreen){
				inputController.changeScreen(0);
				inputController.getMainController().resetGameScreen();
			}
			
			
		}
		if(arg0.getKeyCode() == KeyEvent.VK_S){
			//inputController.incrementScoreTest(100);
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method 
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}
	
}
