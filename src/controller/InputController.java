package controller;

import view.GameScreen;
import view.InfoScreen;
import view.IntroScreen;
import view.ScoreScreen;
import view.Screen;
import view.StartScreen;
import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;
import wiiusej.values.Orientation;
import wiiusej.values.RawAcceleration;
import wiiusej.wiiusejevents.physicalevents.IREvent;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;

public class InputController {
	private KeyboardController keyboardController;
	private WiiInputController wiiInputController1;
	private WiiInputController wiiInputController2;

	private Wiimote wiiMote1 = null;
	private Wiimote wiiMote2 = null;

	private boolean player1Ready = false;
	private boolean player2Ready = false;

	private MainController mainController;
	private boolean player1Start = false;
	private boolean player2Start = false;

	private boolean player1Done = false;
	private boolean player2Done = false;

	public InputController(MainController mainController) {
		this.mainController = mainController;

		try {
			Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(2, true);
			if (wiimotes.length < 2) {
				wiiInputController1 = new WiiInputController(this);
				wiiMote1 = wiimotes[0];
				wiiMote1.addWiiMoteEventListeners(wiiInputController1);
				wiiMote1.activateMotionSensing();
				wiiMote1.setSensorBarAboveScreen();
				wiiMote1.activateIRTRacking();
				wiiMote1.setIrSensitivity(2147483647);
			} else {
				wiiInputController1 = new WiiInputController(this);
				wiiInputController2 = new WiiInputController(this);
				wiiMote1 = wiimotes[0];
				wiiMote2 = wiimotes[1];
				wiiMote1.addWiiMoteEventListeners(wiiInputController1);
				wiiMote2.addWiiMoteEventListeners(wiiInputController2);
				wiiMote1.activateMotionSensing();
				wiiMote2.activateMotionSensing();
				wiiMote1.setSensorBarAboveScreen();
				wiiMote2.setSensorBarAboveScreen();
				wiiMote1.activateIRTRacking();
				wiiMote2.activateIRTRacking();
				wiiMote1.setIrSensitivity(2147483647);
				wiiMote2.setIrSensitivity(2147483647);
			}
		} catch (Exception e) {

		}

		// ArduinoController arduinoController = new ArduinoController();
		keyboardController = new KeyboardController(this);
		mainController.addKeyboardListener(keyboardController);
	}

	public void changeScreen(int screenID) {
		mainController.changeScreen(screenID);
	}

	public Screen getScreen() {
		return mainController.getScreen();
	}

	public void handleIRInput(IREvent irInput, int wiimoteId) {
		mainController.updateIRPosition(irInput.getAx(), irInput.getAy(),
				wiimoteId);
	}

	public void handleButtonInput(WiimoteButtonsEvent wiiInput, int wiimoteId) {
		// Gamescreen section
		if (mainController.getScreen() instanceof GameScreen) {
			if (wiiInput.isButtonAHeld()) {
				mainController.handleGrabInput(wiimoteId);
			}

			if (wiiInput.isButtonAJustReleased()) {
				mainController.handleGrabReleasedInput(wiimoteId);
			}

			// //Testing scores
			// if(wiiInput.isButtonBJustPressed())
			// {
			// mainController.testEffects();
			// }
		}

		// Startscreen section
		if (mainController.getScreen() instanceof StartScreen) {
			if (wiimoteId == 1) {
				if (wiiInput.isButtonOneHeld() && wiiInput.isButtonTwoHeld()) {
					player1Ready = true;
				}
			} else {
				if (wiiInput.isButtonOneHeld() && wiiInput.isButtonTwoHeld()) {
					player2Ready = true;
				}
			}
			
			if (player1Ready && player2Ready) {
				player1Ready = false;
				player2Ready = false;

				mainController.changeScreen(4);
				((IntroScreen)mainController.getScreen()).start();
				mainController.setScore(300);
				mainController.resetScoreDecrementTimer();
			} else {
				player1Ready = false;
				player1Ready = false;
			}
		}
		
		//Intro movie
		if(mainController.getScreen() instanceof IntroScreen)
		{
			boolean ready = false;

			if(wiiInput.isButtonAJustPressed())
			{
				ready = true;
			}
	
			if(ready)
			{
				IntroScreen stuff = ((IntroScreen)mainController.getScreen()); 
				stuff.reset();
				mainController.changeScreen(1);
			}
		}

		// Infoscreen Section
		if (mainController.getScreen() instanceof InfoScreen) {
			if (wiimoteId == 1) {
				if (wiiInput.isButtonAHeld() && wiiInput.isButtonBHeld()) {
					player1Start = true;
				}
			}

			if (wiiInput.isButtonAHeld() && wiiInput.isButtonBHeld()) {
				player2Start = true;
			}

			if (player1Start && player2Start) {
				player1Start = false;
				player2Start = false;
				InfoScreen infoScreen = (InfoScreen)mainController.getScreen();
				infoScreen.resetInfoScreen();
				mainController.changeScreen(2);
			} else {
				player1Start = false;
				player2Start = false;
			}
			if (wiiInput.isButtonAJustReleased()) {
				if(mainController.continueInfoScreen() == false)
				{
					
				}
				else
				{
					mainController.changeScreen(2);
				}
			}
		}
		if (mainController.getScreen() instanceof ScoreScreen) {
			ScoreScreen scoreScreen = (ScoreScreen) mainController.getScreen();
			scoreScreen.changeName(wiimoteId, wiiInput);

			if (wiimoteId == 1) {
				if (wiiInput.isButtonAJustPressed()) {
					player1Done = true;
				}
			}

			if (wiimoteId == 2) {
				if (wiiInput.isButtonAJustPressed()) {
					player2Done = true;
				}
			}
			if (player1Done && player2Done) {

				player1Done = false;
				player2Done = false;
				scoreScreen.commitHighscore();
				mainController.resetGameScreen();
				mainController.changeScreen(0);
			}
		}
	}

	public void handleMotionInput(Orientation orientation, int wiimoteId) {
		// TODO Auto-generated method stub
		mainController.updateMotionInput(orientation, wiimoteId);
	}

	public void handleAccelerationInput(RawAcceleration acceleration,
			int wiimoteId) {
		mainController.updateRawAcceleration(acceleration, wiimoteId);
	}

	public MainController getMainController() {
		return mainController;
	}
}
