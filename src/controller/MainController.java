package controller;

import model.GameModel;
import model.ScreenManager;
import view.DisplayWindow;
import view.GameScreen;
import view.Screen;
import wiiusej.values.Orientation;
import wiiusej.values.RawAcceleration;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;

public class MainController
{

	private DisplayWindow view;
	private GameModel model;

	public MainController(DisplayWindow view, GameModel model)
	{
		// TODO Auto-generated constructor stub
		this.view = view;
		this.model = model;

		InputController inputController = new InputController(this);
	}

	public void addKeyboardListener(KeyboardController keyboardController)
	{
		// TODO Auto-generated method stub
		view.addKeyListener(keyboardController);
	}

	public void changeScreen(int screenID)
	{
		model.setScreen(screenID);
		if(model.getScreen() instanceof GameScreen){
			GameScreen screen = (GameScreen) model.getScreen(); 
			model.addPhysicsObjects(screen.getWorldObjects());
		}
	}
	
	public Screen getScreen()
	{
		return model.getScreen();
	}

	public void updateIRPosition(int x, int y, int wiimoteId)
	{
		model.updateIRPosition(x, y, wiimoteId);
	}

	public void updateMotionInput(Orientation orientation, int wiimoteId)
	{
		// TODO Auto-generated method stub
		model.updateMotionInput(orientation, wiimoteId);
	}
	
	public void updateRawAcceleration(RawAcceleration acceleration, int wiimoteId)
	{
		model.updateRawAcceleration(acceleration, wiimoteId);
	}
	
	public void handleGrabInput(int wiimoteId)
	{
		model.handleGrabInput(wiimoteId);
	}
	
	public void handleGrabReleasedInput(int wiimoteId)
	{
		model.handleGrabReleasedInput(wiimoteId);
	}

	public void testEffects()
	{
		// TODO Auto-generated method stub
		model.testEffects();
	}
	
	public void setScore(int score){
		model.setScore(score);
	}
	
	public void resetScoreDecrementTimer(){
		model.resetScoreDecrementTimer();
	}
	
	public void resetGameScreen(){
		model.resetScoreSystem();
		model.resetMixer();
		model.resetPhysics();
		model.resetGameScreen();
	}

	public boolean continueInfoScreen() {
		// TODO Auto-generated method stub
		return model.continueInfoScreen();
	}
}