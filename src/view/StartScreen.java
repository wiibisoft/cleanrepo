package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class StartScreen extends Screen
{

	private BufferedImage backgroundImage;
	
	public StartScreen()
	{
		super(0);

		try
		{
			backgroundImage = ImageIO.read(new File("res/intro_screen_bg.png"));

		}
		catch (IOException e)
		{
			System.out.println("image was not found in the res folder");
		}
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawImage(backgroundImage, 0, 0, null);
	}

}