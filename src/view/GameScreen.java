package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import model.EasterList;
import model.Mixer;
import model.MusicPlayer;

public class GameScreen extends Screen
{
	private List<WorldObject> worldObjects = new LinkedList<WorldObject>();
	private Hand handL = null;
	private Hand handR = null;
	private Image backgroundImage;
	private boolean leftHandOccupied;
	private boolean rightHandOccupied;
	private ArrayList<EasterPlay> easters = new ArrayList<EasterPlay>();
	private ArrayList<FadeInOut> fadeObjects = new ArrayList<FadeInOut>();
	private int maxFadingObjects = 50;
	private int score;
	private Kettle kettle;
	private boolean kettleLockL = false;
	private boolean kettleLockR = false;
	private boolean potionLockL = false;
	private boolean potionLockR = false;
	private Mixer mixer;
	private Thread fireSound = new Thread(new MusicPlayer("res/music/fire.wav"));
	//EasterPlay easter=new EasterPlay();
	
	public GameScreen(Mixer mixer)
	{
		super(2);

		
//		try
//		{
//			backgroundImage = ImageIO.read(new File(
//					"res/Main.gif"));
//			
//
//		}
//		catch (IOException e)
//		{
//			System.out.println("intro.gif was not found in the res folder");
//		}
		backgroundImage = Toolkit.getDefaultToolkit().createImage("res/videos/Main_v2.gif");
		
		Floor floor = new Floor(0, 675);
		worldObjects.add(floor);

		Shelf shelf1 = new Shelf(240,265, 1); // 1 = l , 2 = mid , 3 = Rechts
		worldObjects.add(shelf1);
		
		Shelf shelf2 = new Shelf(260,398, 1); // 1 = l , 2 = mid , 3 = Rechts
		worldObjects.add(shelf2);
		
		Shelf shelf3 = new Shelf(1030, 465, 3);
		worldObjects.add(shelf3);
		
		Shelf shelf4 = new Shelf(1000, 345, 3);
		worldObjects.add(shelf4);
		
		Shelf shelf5 = new Shelf(640, 120, 1);
		worldObjects.add(shelf5);


		Potion potion1 = new Potion(200, 200, "plsh1", 139, 0, 139);
		worldObjects.add(potion1);
		Potion potion2 = new Potion(300, 200, "plsh2", 0, 0, 255);
		worldObjects.add(potion2);
		Potion potion7 = new Potion(190, 300, "plsh7", 153, 50, 204);
		worldObjects.add(potion7);
		Potion potion8 = new Potion(350, 300, "plsh8", 205, 133, 63);
		worldObjects.add(potion8);
		Potion potion9 = new Potion(125, 300, "plsh9", 128, 128, 0);
		worldObjects.add(potion9);
		
		Potion potion3 = new Potion(950, 400, "plsh3", 255, 0, 0);
		worldObjects.add(potion3);
		Potion potion4 = new Potion(1100, 400, "plsh4", 0, 255, 0);
		worldObjects.add(potion4);

		Potion potion5 = new Potion(1150, 300, "plsh5", 0, 128, 128);
		worldObjects.add(potion5);
		Potion potion6 = new Potion(900, 300, "plsh6", 100, 149, 237);
		worldObjects.add(potion6);
		
		
		kettle = new Kettle(540, 400);
		worldObjects.add(kettle);
		
		handL = new Hand(100, 300, 1);
		worldObjects.add(handL);
		handR = new Hand(800, 300, 2);
		worldObjects.add(handR);

		
		mixer.setKettle(kettle);
		this.mixer = mixer;
		
		fireSound.start();
	}

	public void setHandPosition(int xPos, int yPos, int wiiId)
	{
		if (wiiId == 1)
		{
			if(kettleLockL == false && potionLockL == false)
			{
				handL.setxPos(xPos);
				handL.setyPos(yPos);
			}
			
		}
		else if (wiiId == 2)
		{
			
			if(kettleLockR == false && potionLockR == false)
			{
				handR.setxPos(xPos);
				handR.setyPos(yPos);
			}
		}
	}

	public void setHandAngle(double angle, int wiiId)
	{
		// TODO Auto-generated method stub
		if (wiiId == 1)
		{
			handL.setRotation(angle);
		}
		else if (wiiId == 2)
		{
			handR.setRotation(angle);
		}
	}

	public void setObjectGrabbed(int wiiId)
	{
		if (wiiId == 1)
		{
			Point2D point = new Point2D.Double(handL.getxPos(), handL.getyPos());
			handL.toggleGrabbed(wiiId, true);
			for (WorldObject object : worldObjects)
			{
				if (object instanceof Potion)
				{
					Potion pot = (Potion) object;
					if (pot.getShape().contains(point) && !leftHandOccupied
							&& pot.getGrabbedID() != 2)
					{
						pot.setGrabbedID(1);
						leftHandOccupied = true;
					}

					if (pot.getGrabbedID() == 1)
					{
						pot.getShape().getBounds().setRect(handL.getxPos(), handL.getyPos(), 50, 50);
						pot.setyPos(handL.getyPos());
						pot.setRotation(handL.getRotation());
						pot.getCollisionModel().setPosition(handL.getxPos() + 25, handL.getyPos() + 30);
						pot.getCollisionModel().setRotation((float) handL.getRotation());
						pot.getCollisionModel().setGravityEffected(false);
						
						Point2D liquidStreamPos = pot.getStreamPos();
						double liquidStreamSpeed = pot.getStreamSpeed();
						
						if (liquidStreamPos.getX() > (kettle.getxPos()+25) &&
							liquidStreamPos.getX() < (kettle.getxPos()+175) &&
							liquidStreamPos.getY() < kettle.getyPos() &&
							liquidStreamSpeed > 0.0){
							mixer.addPotion(pot, liquidStreamSpeed);
						}
					}
				}
				
				if(object instanceof Kettle)
				{
					Kettle kettle = (Kettle) object;
					if(kettle.getShape().contains(point) && !leftHandOccupied)
					{
						kettle.setLeftHandGrabbed(true);
						leftHandOccupied = true;
						kettleLockL = true;
					}
				}
			}
		}
		else if (wiiId == 2)
		{
			Point2D point = new Point2D.Double(handR.getxPos(), handR.getyPos());
			handR.toggleGrabbed(wiiId, true);
			
			for (WorldObject object : worldObjects)
			{
				if (object instanceof Potion)
				{
					Potion pot = (Potion) object;
					if (pot.getShape().contains(point) && !rightHandOccupied
							&& pot.getGrabbedID() != 1)
					{
						pot.setGrabbedID(2);
						rightHandOccupied = true;
					}

					if (pot.getGrabbedID() == 2)
					{
						pot.setxPos(handR.getxPos());
						pot.setyPos(handR.getyPos());
						pot.setRotation(handR.getRotation());
						pot.getCollisionModel().setPosition(handR.getxPos() + 25, handR.getyPos() + 30);
						pot.getCollisionModel().setRotation((float) handR.getRotation());
						pot.getCollisionModel().setGravityEffected(false);
						
						Point2D liquidStreamPos = pot.getStreamPos();
						double liquidStreamSpeed = pot.getStreamSpeed();
						
						if (liquidStreamPos.getX() > (kettle.getxPos()+25) &&
							liquidStreamPos.getX() < (kettle.getxPos()+175) &&
							liquidStreamPos.getY() < kettle.getyPos() &&
							liquidStreamSpeed > 0.0){
							mixer.addPotion(pot, liquidStreamSpeed);
						}
					}
				}
				
				if(object instanceof Kettle)
				{
					Kettle kettle = (Kettle) object;
					if(kettle.getShape().contains(point) && !rightHandOccupied)
					{
						kettle.setRightHandGrabbed(true);
						rightHandOccupied = true;
						kettleLockR = true;
					}
					
					if(kettle.getLeftHandGrabbed() && kettle.getRightHandGrabbed())
					{
						handL.setxPos(kettle.getShakePos() - 25);
						handL.setyPos(kettle.getyPos() + 50);
						handR.setxPos(kettle.getShakePos() + kettle.getWidth() - 25);
						handR.setyPos(kettle.getyPos() + 50);
					}
				}
			}
		}
	}

	public void setObjectReleased(int wiiId)
	{
		if (wiiId == 1)
		{
			handL.toggleGrabbed(wiiId, false);
			for (WorldObject object : worldObjects)
			{
				if (object instanceof Potion)
				{
					if (object.getGrabbedID() == 1)
					{
						Potion pot = (Potion) object;
						pot.setGrabbedID(0);
						leftHandOccupied = false;
						pot.getCollisionModel().setGravityEffected(true);
					}
				}
				
				if(object instanceof Kettle)
				{
					Kettle kettle = (Kettle) object;
					if(kettle.getLeftHandGrabbed())
					{
						kettle.setLeftHandGrabbed(false);
						leftHandOccupied = false;
						kettleLockL = false;
					}
				}
			}
		}
		else if (wiiId == 2)
		{
			handR.toggleGrabbed(wiiId, false);
			for (WorldObject object : worldObjects)
			{
				if (object instanceof Potion)
				{
					if (object.getGrabbedID() == 2)
					{
						Potion pot = (Potion) object;
						pot.setGrabbedID(0);
						rightHandOccupied = false;
						pot.getCollisionModel().setGravityEffected(true);
					}
				}
				
				if(object instanceof Kettle)
				{
					Kettle kettle = (Kettle) object;
					if(kettle.getRightHandGrabbed())
					{
						kettle.setRightHandGrabbed(false);
						rightHandOccupied = false;
						kettleLockR = false;
					}
				}
			}
		}
	}

	public void paintComponent(Graphics g)
	{

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		RenderingHints qualityHints =
		new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
						   RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_ANTIALIASING,
						 RenderingHints.VALUE_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_RENDERING,
						 RenderingHints.VALUE_RENDER_QUALITY); // on poor graphical performance set to: VALUE_RENDER_SPEED
		
		g2d.setRenderingHints(qualityHints);
		
		g2d.drawImage(backgroundImage, 0, 0, null);

		ArrayList<Potion> potions = new ArrayList<Potion>();
		ArrayList<Hand> hands = new ArrayList<Hand>();
		Kettle kettle = null;
		
		for (WorldObject object : worldObjects)
		{
			if (object instanceof Potion){
				potions.add((Potion)object);
			} else if (object instanceof Kettle){
				kettle = (Kettle)object;
			} else if (object instanceof Hand){
				hands.add((Hand)object);
			} else if (!(object instanceof Floor)){
				object.draw(g2d);;
			}
		}
		
		kettle.draw(g2d);
		
		for (Hand object : hands){
			if (object.isGrabbed()){
				object.drawGrabbed(g2d);
			}
		}
		
		for (Potion object : potions){
			object.draw(g2d);
		}
		
		for (Hand object : hands){
			if (object.isGrabbed()){
				object.drawFront(g2d);
			}
		}
	
		kettle.draw2(g2d);
		
		for (Hand object : hands){
			if (!object.isGrabbed()){
				object.draw(g2d);
			}
		}
		
		g2d.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 25));
		g2d.setColor(Color.YELLOW);
		g2d.drawString("Score: " + score, 25, 25);

		Iterator<FadeInOut> fadeInOutIterator = fadeObjects.iterator();
		while (fadeInOutIterator.hasNext())
		{
			if (fadeInOutIterator.next().update())
			{
				
			}
			else
			{
				fadeInOutIterator.remove();
			}
		}

		for (FadeInOut fadeObject : fadeObjects)
		{
			
			fadeObject.update();
			fadeObject.draw(g2d);
		
		}

		for (EasterPlay e : easters){
			e.draw(g2d);
			}
		
	}
	int i=0;
	
	public void removePotions(){
		for (Iterator it = worldObjects.iterator(); it.hasNext();) {
			WorldObject object = (WorldObject) it.next();
			if(object instanceof Potion){
				Potion potion = (Potion) object;
				if(!potion.getDraw()){
					it.remove();
				}
			}
		}
	}
	
	public void addEaster(EasterPlay easter){
		easters.add(easter);
	}
	
	public void updateEaster(){
		int idx=0;
		while (idx<easters.size()){
			EasterPlay easter = easters.get(idx);
			easter.update();
			if (easter.infinished()){
				easters.remove(idx);
			}
			else {
				idx++;
			}
		}
//		
//		for (Iterator it = easters.iterator(); it.hasNext();) {
//			EasterPlay easter = (EasterPlay) it.next();
//			easter.update();
//			if (easter.update()){
//				System.out.println("blijf ik wie ik ben");
//				it.remove();
//			}
//		}
	}
	
	public void addPotion(Potion potion){
		worldObjects.add(potion);
	}

	public void setKettleColor(Color color)
	{
		kettle.setColor(color);
	}
	
	public Kettle getKettle()
	{
		return kettle;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public void addFadeObject(FadeInOut fadeObject)
	{
		if(fadeObjects.size() < maxFadingObjects){
			fadeObjects.add(fadeObject);
		}
	}
	
	public List<WorldObject> getWorldObjects(){
		return worldObjects;
	}

	public boolean getLeftHandOccupied() {
		return leftHandOccupied;
	}

	public void setLeftHandOccupied(boolean leftHandOccupied) {
		this.leftHandOccupied = leftHandOccupied;
	}

	public boolean getRightHandOccupied() {
		return rightHandOccupied;
	}

	public void setRightHandOccupied(boolean rightHandOccupied) {
		this.rightHandOccupied = rightHandOccupied;
	}

	public boolean getKettleLockL() {
		return kettleLockL;
	}

	public void setKettleLockL(boolean kettleLockL) {
		this.kettleLockL = kettleLockL;
	}

	public boolean getKettleLockR() {
		return kettleLockR;
	}

	public void setKettleLockR(boolean kettleLockR) {
		this.kettleLockR = kettleLockR;
	}

	public boolean getPotionLockL() {
		return potionLockL;
	}

	public void setPotionLockL(boolean potionLockL) {
		this.potionLockL = potionLockL;
	}

	public boolean getPotionLockR() {
		return potionLockR;
	}

	public void setPotionLockR(boolean potionLockR) {
		this.potionLockR = potionLockR;
	}
}
