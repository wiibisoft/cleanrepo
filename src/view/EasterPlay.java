package view;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class EasterPlay extends WorldObject {

	private BufferedImage[] sprites = new BufferedImage[6]; // the images that are loaded in from the sprite
	private int spritecount = 0, // the displayed spriteImage
			rows = 1, // // number of rows (horizontal lines) in the sprite
			colums = 6, // number of colum (vertical lines) in the sprite
			easterId = 0, // the id of the specific easter
			hight = 150, // the hight of a single image you wanne load in
			Width = 160, // the width of a single image you wanne load in
			animatieSpeed = 5, // the number of steps 1 frame/image last
			posX = 0,// the left side of the image
			posY = 0, // the bottem corner of the image
			animationCount = 0, // number of updates made on this easter
			imageHight = 150, // the hight of the image that has to been shown
								// on screen
			imageWidth = 160; // the width of the image that has to been shown
								// on screen

	private boolean finished = false;

	private static int ResizeImageHight = 150, resizeImageWidth = 160; // used to resize the painted image

	public EasterPlay(int i) {
		super(0, 0);
		easterId = i;
		switch (i) {
		case 0:
			Load0();
			break;
		case 1:
			Load1();
			break;
		case 2:
			Load2();
			break;
		case 3:
			Load3();
			break;
		case 4:
			Load4();
			break;
		case 5:
			Load5();
			break;
		default:
			Load1();
			break;
		}
	}
	
	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 * 
	 * nuke animation
	 */
	private void Load0() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 21; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed =5; // the number of steps 1 frame/image last
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter0.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		
		
		sprites[0] = bigImg.getSubimage(0,0, 60,
				100);
		sprites[1] = bigImg.getSubimage(60,0, 144-60,
				100);
		sprites[2] = bigImg.getSubimage(144,0, 258-144,
				100);
		sprites[3] = bigImg.getSubimage(258,0, 373-258,
				100);
		sprites[4] = bigImg.getSubimage(373,0, 490-373,
				100);
		sprites[5] = bigImg.getSubimage(4,100, 122-4,
				222-100);
		sprites[6] = bigImg.getSubimage(135,100, 250-135,
				222-100);
		sprites[7] = bigImg.getSubimage(258,100, 366-258,
				222-100);
		sprites[8] = bigImg.getSubimage(384,100, 488-384,
				222-100);
		sprites[9] = bigImg.getSubimage(488,100, 600-488,
				222-100);
		sprites[10] = bigImg.getSubimage(2,230, 110-2,
				352-230);
		sprites[11] = bigImg.getSubimage(120,230, 240-120,
				360-230);
		sprites[12] = bigImg.getSubimage(240,230, 360-240,
				366-230);
		sprites[13] = bigImg.getSubimage(366,230, 490-366,
				367-230);
		sprites[14] = bigImg.getSubimage(4,370, 128-4,
				500-370);
		sprites[15] = bigImg.getSubimage(140,383, 270-140,
				512-383);
		sprites[16] = bigImg.getSubimage(270,384, 394-270,
				525-384);
		sprites[17] = bigImg.getSubimage(410,384, 525-410,
				525-384);
		sprites[18] = bigImg.getSubimage(8,533, 120-8,
				635-533);
		sprites[19] = bigImg.getSubimage(145,533, 250-145,
				635-533);
		sprites[20] = bigImg.getSubimage(277,533, 365-277,
				635-533);
		
		imageHight = 150;
		imageWidth = 160;
		posY = 470;
		posX = 1280/2;
	}


	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 * 
	 * bigfood
	 */
	private void Load1() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 6; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed = 5; // the number of steps 1 frame/image last

		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter1.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		for (int i = 0; i < colums; i++) {
			for (int j = 0; j < rows; j++) {

				sprites[i] = bigImg.getSubimage(i * Width, j * hight, Width,
						hight);
			}
		}
		imageHight = 150;
		imageWidth = 160;
		posY = 400;
		posX = -imageWidth;
	}

	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 * 
	 * smal dragon
	 */
	private void Load2() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 5; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed = 5; // the number of steps 1 frame/image last
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter2.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		for (int i = 0; i < colums; i++) {
			for (int j = 0; j < rows; j++) {

				sprites[i] = bigImg.getSubimage(i * Width, j * hight, Width,
						hight);
			}
		}
		imageHight = 25 * 3;
		imageWidth = 24 * 3;
		posY = 400;
		posX = -imageWidth;
	}

	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 */
	private void Load3() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 6; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed = 5; // the number of steps 1 frame/image last
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter1.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		for (int i = 0; i < colums; i++) {
			for (int j = 0; j < rows; j++) {

				sprites[i] = bigImg.getSubimage(i * Width, j * hight, Width,
						hight);
			}
		}

		imageHight = 150;
		imageWidth = 160;
		posY = 400;
		posX = -imageWidth;
	}

	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 */
	private void Load4() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 6; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed = 5; // the number of steps 1 frame/image last
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter1.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		for (int i = 0; i < colums; i++) {
			for (int j = 0; j < rows; j++) {

				sprites[i] = bigImg.getSubimage(i * Width, j * hight, Width,
						hight);
			}
		}
		imageHight = 150;
		imageWidth = 160;
		posY = 400;
		posX = -imageWidth;
	}

	/*
	 * this is the standart methode for an sprite sheet where al the subImages
	 * are the same size
	 */
	private void Load5() {
		rows = 1; // number of rows (horizontal lines) of images
		colums = 6; // number of colum (vertical lines) of images
		sprites = new BufferedImage[rows * colums]; // the images that are
													// loaded in from the sprite
		animatieSpeed = 5; // the number of steps 1 frame/image last
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File("res/easter1.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null;
		}

		Width = bigImg.getWidth() / colums; // the width of a single image you
											// wanne load in
		hight = bigImg.getHeight() / rows; // the hight of a single image you
											// wanne load in
		for (int i = 0; i < colums; i++) {
			for (int j = 0; j < rows; j++) {

				sprites[i] = bigImg.getSubimage(i * Width, j * hight, Width,
						hight);
			}
		}
		imageHight = 150;
		imageWidth = 160;
		posY = 400;
		posX = -imageWidth;
	}

	
	/*
	 * lead the easter to the wright easter update
	 */
	public void update() {
		switch (easterId) {
		case 0:
			finished = updateOnTopOfKettle();
			break;
		case 1:
			finished = updateWalk();
			break;
		case 2:
			finished = updateWalk();
			break;
		case 3:
			finished = updateWalk();
			break;
		case 4:
			finished = updateWalk();
			break;
		default:
			finished = updateWalk();
			break;
		}
	}
	
	public boolean infinished(){
		return finished;
	}

	/*
	 * the update for walking from left to wright
	 */
	private boolean updateWalk() {
		animationCount++;
		if (animationCount % animatieSpeed == 0) {
			spritecount++;
		}

		posX++;
		if (posX > 1280) {
			return true;
		}
		return false;
	}
	
	private boolean updateOnTopOfKettle(){
		
		animationCount++;
		if (animationCount % animatieSpeed == 0) {
			spritecount++;
			if (spritecount==17){
				posY-=10;
			}
			if (spritecount==18){
				posY-=10;
			}
			if (spritecount==19){
				posY-=10;
			}
			if (spritecount==20){
				posY-=10;
			}
			if (spritecount==21){
				posY-=10;
			}
		}
		
		
		
		if ((spritecount % (rows*colums)==0)&&(animationCount % animatieSpeed == 0)) {
			return true;
		}
		return false;
		
	}

	/*
	 * lead the easter to the wright easter draw
	 */
	public void draw(Graphics2D g2d) {
		switch (easterId) {
		case 0:
			justDraw(g2d);
			break;
		case 1:
			drawWalkBy(g2d);
			break;
		case 2:
			drawWalkBy(g2d);
			break;
		case 3:
			drawWalkBy(g2d);
			break;
		case 4:
			drawWalkBy(g2d);
			break;
		case 5:
			drawWalkBy(g2d);
			break;
		default:
			drawWalkBy(g2d);
			break;
		}
	}

	/*
	 * the drawing for waling from left to wright
	 */
	public void drawWalkBy(Graphics2D g2d) {
		resizeImageWidth = imageWidth;
		ResizeImageHight = imageHight;

		AffineTransform transform = new AffineTransform();
		transform.translate(posX, posY - imageHight);
		int type = sprites[(spritecount / 5) % (rows * colums)].getType() == 0 ? BufferedImage.TYPE_INT_ARGB
				: sprites[(spritecount / 5) % (rows * colums)].getType();
		g2d.drawImage(
				resizeImage(sprites[(spritecount) % (rows * colums)], type),
				transform, null);

	}
	
	public void justDraw(Graphics2D g2d){
		
		ResizeImageHight = sprites[(spritecount) % (rows * colums)].getHeight()*3;
		resizeImageWidth = sprites[(spritecount) % (rows * colums)].getWidth()*3;
		int type = sprites[(spritecount / 5) % (rows * colums)].getType() == 0 ? BufferedImage.TYPE_INT_ARGB
				: sprites[(spritecount / 5) % (rows * colums)].getType();
		
		AffineTransform transform = new AffineTransform();
		transform.translate(posX-sprites[(spritecount) % (rows * colums)].getWidth()*1.5, 
				posY - sprites[(spritecount) % (rows * colums)].getHeight()*3);
		g2d.drawImage(
				resizeImage(sprites[(spritecount) % (rows * colums)],type),
				transform, null);
	}

	/*
	 * static methode that resizes the image to paint it
	 */
	private static BufferedImage resizeImage(BufferedImage originalImage,
			int type) {
		BufferedImage resizedImage = new BufferedImage(resizeImageWidth,
				ResizeImageHight, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, resizeImageWidth, ResizeImageHight,
				null);
		g.dispose();

		return resizedImage;
	}

}
