package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Kettle extends WorldObject {
	private Shape s1;
	private int height = 60, width = 160, spritecount=0;
	private Color color = new Color(100, 100, 255,155);
	private int shakeSteps = 0;
	private int shakeLength = 10;
	private int shakePos=0;
	private int shakeTreshhold=125;
	
	private BufferedImage[] sprites = new BufferedImage[5];

	private boolean leftHandGrabbed = false;
	private boolean rightHandGrabbed = false;

	private Image topImage;
	private Image bottomImage;
	/*
	 * constructor
	 * 
	 * @ param int posx ; the x position of the mid point of the kettle
	 * 
	 * @ param int posy ; the y position of the mid point of the kettle
	 */
	public Kettle(int posx, int posy) {
		super(posx, posy);
		try {
			topImage = ImageIO.read(new File("res/kettle_top.png"));
			bottomImage = ImageIO.read(new File("res/kettle_bottom.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		s1 = new Ellipse2D.Double(posx + topImage.getWidth(null) / 2 - width / 2,
				posy + height / 2, width, height);
		

		BufferedImage bigImg;
		
		try {
			bigImg = ImageIO.read(new File("res/potInside.png"));
		} catch (IOException e) {
			e.printStackTrace();
			bigImg = null; 
		}
		for (int i = 0; i < 5; i++) 
		{ 
			sprites[i] = bigImg.getSubimage( 
		            i * 200, 
		            0, 
		            200, 
		            200 
		        ); 
		    
		}
	}

	/*
	 * @ param int posx ; the x position of the mid point of the kettle
	 * 
	 * @ param int posy ; the y position of the mid point of the kettle
	 */
	public void setPosition(int posx, int posy) {
		super.setxPos(posx);
		super.setyPos(posy);
		s1.getBounds().setBounds(posx + topImage.getWidth(null) / 2 - width / 2,
				posy + height / 2, width, height);
	}

	/*
	 * @ return ; the shape to of the kettle
	 */
	public Shape getShape() {
		return s1;
	}

	/*
	 * @ param Color color ; set color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/*
	 * @param Graphics2D g2d ; the graphic plane to draw in
	 */
	/** Draw the top part */
	public void draw(Graphics2D g2d) {
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + shakePos, yPos);
		s1 = new Ellipse2D.Double(xPos + shakePos + topImage.getWidth(null) / 2
				- width / 2, yPos + height / 2, width, height);
		g2d.setColor(color);
		g2d.drawImage(sprites[(spritecount/5)%5], transform, null);
		g2d.fill(s1);
		spritecount++;

		g2d.drawImage(topImage, transform, null);
	}
	
	/** Draw the bottom part */
	public void draw2(Graphics2D g2d){
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + shakePos, yPos);
		g2d.drawImage(bottomImage, transform, null);
	}
	
	
	public boolean shake(){
		
		if (shakePos>50||shakePos<-50){
			shakeLength=-shakeLength;
			shakeSteps++;	
		}
		
		shakePos+=shakeLength;
		
		if (shakeSteps>4&&shakePos==0){
			shakeSteps=0;
			return true;
		}
		return false;
	}

	/*
	 * @ param int p1x : the x psition of player 1
	 * 
	 * @ param int p2x : the x psition of player 2
	 */
	public boolean checkShake(int p1x,int p2x)
	{
		if (leftHandGrabbed&&rightHandGrabbed)
		{
			shakeTreshhold=125;
			if ((p1x<-shakeTreshhold||p1x>shakeTreshhold)
					&&(p2x<-shakeTreshhold||p2x>shakeTreshhold))
			{

					return shake();
			}
		}
		return false;
	}

	public void setLeftHandGrabbed(boolean grabbed) {
		leftHandGrabbed = grabbed;
	}

	public void setRightHandGrabbed(boolean grabbed) {
		rightHandGrabbed = grabbed;
	}

	public boolean getLeftHandGrabbed() {
		return leftHandGrabbed;
	}

	public boolean getRightHandGrabbed() {
		return rightHandGrabbed;
	}

	public float getShakePos() {
		return xPos + shakePos;
	}

	public float getWidth() {
		return topImage.getWidth(null);
	}
}
