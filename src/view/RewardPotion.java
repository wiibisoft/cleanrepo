package view;

public class RewardPotion extends Potion
{
	private boolean glow;
	
	/*
	 * constructor
	 * @ param String name ; name of the potion
	 * @ param int red ; the number of red the potion contains
	 * @ param int green ; the number of green the potion contains
	 * @ param int blue ; the number of blue the potion contains
	 * 
	 * sets glow on
	 */
	public RewardPotion(int xPos, int yPos, String name, int red, int green, int blue) {
		super(xPos, yPos, name, red, green, blue);
		glow = true;
	}
}
