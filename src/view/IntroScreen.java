package view;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.Timer;

public class IntroScreen extends Screen implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	ArrayList<BufferedImage> frames = new ArrayList<BufferedImage>();
    int currentFrame = 0;
    Timer timer = new Timer(1000/15, this);
    Timer waitTime = new Timer(10000, this);
    
	public IntroScreen()
	{
		super(4);
		File folder = new File("res/videos/intro_video/");
		for (File frame : folder.listFiles()){
			BufferedImage image = null;
			
			try {
				image = ImageIO.read(frame);
			} catch (IOException e) {e.printStackTrace();}
			
			frames.add(image);
		}
	}
	
	public void start(){
		timer.start();
	}

	public void reset(){
		timer.stop();
		currentFrame = 0;
	}
	
	public void paintComponent(Graphics g)
	{

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		RenderingHints qualityHints =
		new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
						   RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_ANTIALIASING,
						 RenderingHints.VALUE_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_RENDERING,
						 RenderingHints.VALUE_RENDER_QUALITY); // on poor graphical performance set to: VALUE_RENDER_SPEED
		
		g2d.setRenderingHints(qualityHints);

		g2d.drawImage(frames.get(currentFrame), 0, -20, null);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (timer.isRunning())timer.stop();
		if (waitTime.isRunning()) waitTime.stop();
		
		currentFrame++;
		repaint();
		if (currentFrame >= frames.size()-2){
			currentFrame = frames.size()-2;
		}
		
		if (currentFrame == 35 || currentFrame == 75 || currentFrame == 118){
			waitTime.start();
		} else { timer.start(); }
		
	}
}
