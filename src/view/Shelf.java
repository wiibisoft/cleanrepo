package view;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import view.WorldObject;

public class Shelf extends WorldObject {
	private java.awt.Shape shape; //

	public Shelf(int xPos, int yPos, int number) {
		super(xPos, yPos);

		if (number == 1) {
			try {
				image = ImageIO.read(new File("res/plankL_dark.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (number == 2) {
			try {
				image = ImageIO.read(new File("res/plank_dark.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (number == 3) {
			try {
				image = ImageIO.read(new File("res/plankR_dark.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void draw(Graphics2D g2d) {
		// TODO Auto-generated method stub
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos, yPos);

		g2d.drawImage(image, transform, null);
//		g2d.drawImage(image, null, null);
	}
}