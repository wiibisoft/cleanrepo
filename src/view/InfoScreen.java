package view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class InfoScreen extends Screen {
	
	private BufferedImage introImage1 = new BufferedImage(240,240, BufferedImage.TYPE_INT_ARGB);
	private BufferedImage introImage2 = new BufferedImage(240,240, BufferedImage.TYPE_INT_ARGB);
	private BufferedImage introImage3 = new BufferedImage(240,240, BufferedImage.TYPE_INT_ARGB);
	private int currScreen = 1;
	
	public InfoScreen(){
		super(1);
		try {
			introImage1 = ImageIO.read(new File("res/intro1.png"));
			introImage2 = ImageIO.read(new File("res/intro2.png"));
			introImage3 = ImageIO.read(new File("res/intro3.png"));
			
		} catch (IOException e) {
			System.out.println("intro.gif was not found in the res folder");
		}
	}
	
public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		switch(currScreen )
		{
		case 1:
		g2d.drawImage(introImage1, 0, 0, null);
		break;
		
		case 2:
			g2d.drawImage(introImage2, 0, 0, null);
			break;
		case 3:
			g2d.drawImage(introImage3, 0, 0, null);
			break;
		}
	}

public boolean continueScreen() {
	// TODO Auto-generated method stub
	if(currScreen < 3)
	{
		currScreen++;
		return false;
	}
	else
	{
		return true;
	}
}
public void resetInfoScreen(){
	currScreen =1;
}
}
