package view;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import net.phys2d.raw.Body;
import net.phys2d.raw.StaticBody;
import net.phys2d.raw.shapes.Box;

public abstract class WorldObject { // at the creation of a WorldObject a shape must be supplied to the collsionModel for... collision model purposes..
	protected double rotation;
	protected float xPos;
	protected float yPos;
//	protected boolean grabbed = false;
	private Body collisionModel;
	private int dimension = 2; // 1 for fore ground, 2 for background
	protected BufferedImage image;
	protected int grabbedByWiiID;
	
	public WorldObject(int xPos, int yPos){
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	public abstract void draw(Graphics2D g2d);
	
	public Body getCollisionModel(){
		return collisionModel;
	}
	public void addCollisionModel(int width, int height, int weight, boolean $static){
		if($static){
			collisionModel = new StaticBody(new Box(width, height));
		}
		else{
			collisionModel = new Body(new Box(width, height), weight);
		}
	}
	
	public int getDimension(){
		return dimension;
	}
	
	public void update(){ // updates to the, by the physics engine calculated, position and rotation
			xPos = collisionModel.getPosition().getX() - (image.getWidth()/2);
			yPos = collisionModel.getPosition().getY() - (image.getHeight()/2);
			rotation = collisionModel.getRotation();
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public float getxPos() {
		return xPos;
	}

	public void setxPos(float xPos) {
		this.xPos = xPos;
	}

	public float getyPos() {
		return yPos;
	}

	public void setyPos(float yPos) {
		this.yPos = yPos;
	}

	public int getGrabbedID() {
		return grabbedByWiiID;
	}

	public void setGrabbedID(int wiiID) 
	{
		this.grabbedByWiiID = wiiID;
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}
}