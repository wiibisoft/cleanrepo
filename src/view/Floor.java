package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Floor extends WorldObject{
	private Shape rect = new Rectangle2D.Double(xPos, yPos, 1800, 20);

	public Floor(int xPos, int yPos) {
		super(xPos, yPos);
		
		try {
			image = ImageIO.read(new File("res/plank_dark.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void draw(Graphics2D g2d) {
		g2d.setColor(Color.gray);
		g2d.fill(rect);
	}

	public Shape getShape() {
		return rect;
	}
}
