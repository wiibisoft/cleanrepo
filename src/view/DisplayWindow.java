package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class DisplayWindow extends JFrame implements ActionListener
{
	public DisplayWindow()
	{
		setTitle("Brewing Magic");
        setSize(1280,720);
        //adds view to the frame.
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setFocusable(true);
        
        Timer repaintTimer = new Timer(1000/60, this);
        repaintTimer.start();
	}
	
	public void draw()
	{
		getContentPane().repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		draw();
	}
	
}
