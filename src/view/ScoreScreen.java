package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.Timer;

import model.Highscore;
import model.HighscoreList;
import model.HighscoreManager;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;

public class ScoreScreen extends Screen implements ActionListener
{

	private Image backgroundImage;
	private Image highscore;
	private Image yourscore;
	private HighscoreList highscoreList;
	private ArrayList<Highscore> highscores;
	
	private String[] alphabet =
	{ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
			"O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	private int player1Selected = 1;
	private int player1NameChar1 = 0;
	private int player1NameChar2 = 0;
	private int player1NameChar3 = 0;

	private int player2Selected = 1;
	private int player2NameChar1 = 0;
	private int player2NameChar2 = 0;
	private int player2NameChar3 = 0;

	private int score = 0;

	private Timer timer = new Timer(1000 / 10, this);

	public ScoreScreen()
	{
		super(3);
		highscoreList = HighscoreManager.loadHighscores();
		highscores = highscoreList.getHighscores();
		
		timer.start();

//		try
//		{
//			backgroundImage = ImageIO.read(new File("res/lighted_background.png"));
//
//		}
//		catch (IOException e)
//		{
//			System.out.println("intro.gif was not found in the res folder");
//		}

		backgroundImage = Toolkit.getDefaultToolkit().createImage("res/videos/Main_v2.gif");

		highscore = Toolkit.getDefaultToolkit().createImage("res/highscore.png");
		yourscore = Toolkit.getDefaultToolkit().createImage("res/name-fill-in.png");


	}
	
	private AffineTransform getTransform(int x,int y,double angle) {
		AffineTransform tx = new AffineTransform();
		tx.translate(x-375/2, y-200/2);
		tx.rotate(angle, 375/2, 200/2);
		return tx;
	}

	public void paintComponent(Graphics g)
	{

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		RenderingHints qualityHints =
		new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
						   RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_ANTIALIASING,
						 RenderingHints.VALUE_ANTIALIAS_ON);
		
		qualityHints.put(RenderingHints.KEY_RENDERING,
						 RenderingHints.VALUE_RENDER_QUALITY); // on poor graphical performance set to: VALUE_RENDER_SPEED
		
		g2d.setRenderingHints(qualityHints);
		
		// draws the background
		g2d.drawImage(backgroundImage, 0, 0, null);
		
		// draws the highscore papers
		g2d.drawImage(yourscore, getTransform(280, 150,(Math.PI/100)*5), null);
		g2d.drawImage(highscore, 700, 100, null);
		
		//g2d.drawImage(yourscore, 100, 400, null);

		g2d.drawImage(yourscore, getTransform(280, 480,(Math.PI/100)*95), null);


		g2d.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 25));
		g2d.setColor(Color.YELLOW);
		g2d.setColor(new Color(50,0,0,200));

		g2d.drawString("Player 1", 150, 150);
		g2d.drawString("Player 2", 295, 150);
		g2d.drawString("" + alphabet[player1NameChar1] + "" + alphabet[player1NameChar2]
				+ "" + alphabet[player1NameChar3], 150, 200);
		g2d.drawString("" + alphabet[player2NameChar1] + "" + alphabet[player2NameChar2]
				+ "" + alphabet[player2NameChar3], 295, 200);

		g2d.drawString("Score", 170, 450);
		g2d.drawString(""+score, 170, 500);
		

		g2d.setColor(new Color(30,0,0,200));
		for(int i = 0; i < highscores.size(); i++)
		{
			g2d.drawString(highscores.get(i).getName1(), 755, 30 * i + 225);
			g2d.drawString(highscores.get(i).getName2(), 830, 30 * i + 225);
			g2d.drawString(Integer.toString(highscores.get(i).getHighscore()), 930, 30 * i + 225);
		}
		
		// draw a selection indicator for players
		g2d.draw(new Rectangle2D.Double(130 + (player1Selected * 20), 170, 20,
				30));
		g2d.draw(new Rectangle2D.Double(275 + (player2Selected * 20), 170, 20,
				30));
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	public void changeName(int wiiMoteID, WiimoteButtonsEvent wiiInput)
	{
		switch (wiiMoteID)
		{
		case 1:
			if (wiiInput.isButtonLeftJustPressed())
			{
				selectChar(player1Selected - 1, wiiMoteID);
			}
			if (wiiInput.isButtonRightJustPressed())
			{
				selectChar(player1Selected + 1, wiiMoteID);
			}
			break;
		case 2:
			if (wiiInput.isButtonLeftJustPressed())
			{
				selectChar(player2Selected - 1, wiiMoteID);
			}
			if (wiiInput.isButtonRightJustPressed())
			{
				selectChar(player2Selected + 1, wiiMoteID);
			}
			break;
		default:
			break;
		}
		if (wiiInput.isButtonUpJustPressed())
		{
			mutateName(true, wiiMoteID);
		}
		if (wiiInput.isButtonDownJustPressed())
		{
			mutateName(false, wiiMoteID);
		}

	}

	public void selectChar(int selectedChar, int wiiMoteID)
	{
		if (selectedChar < 1)
		{
			switch (wiiMoteID)
			{
			case 1:
				player1Selected = 1;
				break;
			case 2:
				player2Selected = 1;
				break;
			default:
				break;
			}
		}
		else if (selectedChar > 3)
		{
			switch (wiiMoteID)

			{
			case 1:
				player1Selected = 3;
				break;
			case 2:
				player2Selected = 3;
				break;
			default:
				break;
			}
		}
		else
		{
			switch (wiiMoteID)
			{
			case 1:
				player1Selected = selectedChar;
				break;
			case 2:
				player2Selected = selectedChar;
				break;
			default:
				break;
			}
		}
	}

	public void mutateName(boolean up, int wiiMoteID)
	{
		if (wiiMoteID == 1)
		{
			if (up)
			{
				switch (player1Selected)
				{
				case 1:
					player1NameChar1 += 1;
					break;
				case 2:
					player1NameChar2 += 1;
					break;
				case 3:
					player1NameChar3 += 1;
					break;
				default:
					break;
				}
			}
			else
			{
				switch (player1Selected)
				{
				case 1:
					player1NameChar1 -= 1;
					break;
				case 2:
					player1NameChar2 -= 1;
					break;
				case 3:
					player1NameChar3 -= 1;
					break;
				default:
					break;
				}
			}
		}

		if (wiiMoteID == 2)
		{
			if (up)
			{
				switch (player2Selected)
				{
				case 1:
					player2NameChar1 += 1;
					break;
				case 2:
					player2NameChar2 += 1;
					break;
				case 3:
					player2NameChar3 += 1;
					break;
				default:
					break;
				}
			}
			else
			{
				switch (player2Selected)
				{
				case 1:
					player2NameChar1 -= 1;
					break;
				case 2:
					player2NameChar2 -= 1;
					break;
				case 3:
					player2NameChar3 -= 1;
					break;
				default:
					break;
				}
			}
		}

		if (player1NameChar1 < 0)
		{
			player1NameChar1 = 25;
		}
		if	(player1NameChar1 > 25)
		{
			player1NameChar1 = 0;
		}
		if (player1NameChar2 < 0)
		{
			player1NameChar2 = 25;
		}
		if (player1NameChar2 > 25)
		{
			player1NameChar2 = 0;
		}
		if (player1NameChar3 < 0)
		{
			player1NameChar3 = 25;
		}
		if (player1NameChar3 > 25)
		{
			player1NameChar3 = 0;
		}
		if (player2NameChar1 < 0)
		{
			player2NameChar1 = 25;
		}
		if (player2NameChar1 > 25)
		{
			player2NameChar1 = 0;
		}
		if (player2NameChar2 < 0)
		{
			player2NameChar2 = 25;
		}
		if (player2NameChar2 > 25)
		{
			player2NameChar2 = 0;
		}
		if (player2NameChar3 < 0)
		{
			player2NameChar3 = 25;
		}
		if (player2NameChar3 > 25)
		{
			player2NameChar3 = 0;
		}
	}
	
	public void setScore(int score){
		this.score = score;
	}
	
	
	public void commitHighscore(){
		highscoreList.addHighscore(score, alphabet[player1NameChar1]+alphabet[player1NameChar2]
				+alphabet[player1NameChar3], alphabet[player2NameChar1]+alphabet[player2NameChar2]
						+alphabet[player2NameChar3]);
		HighscoreManager.saveHighscores(highscoreList);
	}
	
	
}
