package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;

public class PointsPopup extends FadeInOut {

	private boolean increase = true;
	private int score = 0;
	
	public PointsPopup(int x, int y, int fadeValue, Color color, int score){
		super(x, y, fadeValue, color);
		this.color = new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), fadeValue);
		this.score = score;
	}
	
	@Override
	 public boolean update() {
		if(increase){
			if(!increaseAlpha()){
				increase = false;
			}
			increaseAlpha();
		}
		if(!increase){
			if(!decreaseAlpha()){
				return false;
			}
			decreaseAlpha();
		}
		
		return true;
	}

	@Override
	public void draw(Graphics g2d) {
		g2d.setColor(color);
		g2d.drawString("" + score, x, y);
	}
}
