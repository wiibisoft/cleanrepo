package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Potion extends WorldObject
{
	private int red,green,blue;
	private String name;
	private GeneralPath fittedShape;
	private Shape shape;
	private double volume = 70; // volume in percentages
	private double streamSpeed = 0;
	
	private Image breakGif = Toolkit.getDefaultToolkit().getImage("res/videos/Potion_break.gif");

	private boolean breaking = false;
	private boolean draw = true;
	
	//draw variables
	private Area liquid = new Area(new Rectangle2D.Double(1,1,1,1));
	private int streamWidth;
	public Point2D streamPos;
	private AffineTransform transform;
	
	/**
	 * constructor
	 * @ param int xPos ; X position on the panel
	 * @ param int yPos ; Y position on the panel
	 * @ param String name ; name of the potion
	 * @ param int red ; the number of red the potion contains
	 * @ param int green ; the number of green the potion contains
	 * @ param int blue ; the number of blue the potion contains
	 **/
	public Potion (int xPos, int yPos, String name,int red,int green,int blue){
		super(xPos, yPos);
		this.name=name;
		this.red=red;
		this.green=green;
		this.blue=blue;
		this.xPos = xPos;
		this.yPos = yPos;
		
		try {
			image = ImageIO.read(new File("res/Potion.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GeneralPath path = new GeneralPath();
		path.moveTo(15, 0);
		path.lineTo(17, 2);
		path.lineTo(17, 19);
		path.lineTo(0, 54);
		path.lineTo(0, 59);
		path.curveTo(2, 63, 2, 63, 6, 65);
		path.lineTo(42, 65);
		path.curveTo(48, 63, 48, 63, 49, 59);
		path.lineTo(49, 52);
		path.lineTo(33, 19);
		path.lineTo(33, 3);
		path.lineTo(36, 0);
		path.closePath();
		
		fittedShape = path;
		shape = new Rectangle2D.Double(xPos, yPos, 50, 50);
		
	}
	
	/** Returns the stream width in the return and the stream position in the provided Point2D */
	public Point2D getStreamPos(){
		return streamPos;
	}
	
	public double getStreamSpeed(){
		return streamSpeed;
	}
	
	@Override
	/** Draw the shape */
	public void draw(Graphics2D g2d) {
		if(draw){
			g2d.setColor(new Color(red, green, blue, 155));
			if(!breaking){
				g2d.fill(liquid);
			}

			if (streamWidth > 0){
				g2d.setStroke(new BasicStroke(streamWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
	
				if (streamPos.getX() > 565 && streamPos.getX() < 715){
					g2d.drawLine((int)streamPos.getX(), (int)streamPos.getY()-(streamWidth/2), (int)streamPos.getX(), 460);
				} else {
					g2d.drawLine((int)streamPos.getX(), (int)streamPos.getY()-(streamWidth/2), (int)streamPos.getX(), 660);
				}
				
			}
			
			if(breaking){
				g2d.drawImage(breakGif, transform, null);
				breaking = false;
			} else{
				g2d.drawImage(image, transform, null);
			}
		}
	}
	
	public void update()
	{
		super.update();
		((Rectangle2D) shape).setRect(xPos - 10, yPos, 70, 70);
		
		// remove liquid from the bottle
		if (volume > 1){
			volume = volume - streamSpeed;
		}
		
		// create the transform for all the shapes
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos+25, yPos+33);
	    transform.rotate(rotation);
	    transform.translate(-xPos-25, -yPos-33);
	    transform.translate(xPos, yPos);
	    
	    // transform the fitted shape
	    Area fittedArea = new Area(this.fittedShape);
	    fittedArea.transform(transform);

	    // create a little square at the top of the bottle
	    // to get coordinates for the fluid stream.
	    Area fittedAreaIntersect = new Area(new Rectangle2D.Double(0, 0, 50, 1));
	    fittedAreaIntersect.transform(transform);
	    fittedAreaIntersect.intersect(fittedArea);
	    
	    // create the fluid rectangle then cut out a shape with the fitted shape
		double liquidHeight = (fittedArea.getBounds2D().getHeight()/100)*volume;
		double liquidHeightDifference = fittedArea.getBounds2D().getHeight() - liquidHeight;
		
		double x = xPos + 25 -(fittedArea.getBounds2D().getWidth()/2);
		double y = (yPos + 33 - (fittedArea.getBounds2D().getHeight()/2))+liquidHeightDifference;
		Rectangle2D liquid = new Rectangle2D.Double(x, y, fittedArea.getBounds2D().getWidth(), liquidHeight+10);
		
		Area fittedLiquid = new Area(liquid);
		fittedLiquid.intersect(fittedArea);
		
	    // get the point where the fluid stream starts
	    PathIterator points = fittedAreaIntersect.getPathIterator(null);
	    double[] coords = new double[6];
	    
	    ArrayList<double[]> list = new ArrayList<double[]>();
	    
	    while(!points.isDone()){
	    	points.currentSegment(coords);
	    		list.add(coords.clone());
	    	    //g2d.drawLine((int)coords[0], (int)coords[1], (int)coords[0], (int)coords[1]+30);

			points.next();
	    }
	    
	    int streamY = 0;
	    int streamX = 0;
	    
	    for (double[] point : list){
	    	if (point[1] > streamY){
	    		streamY = new Integer((int)point[1]);
	    		streamX = new Integer((int)point[0]);
	    	}
	    }
	    
	    // make sure the fluid stream doesn't get to wide.
	    int streamWidth = (int) (streamY-fittedLiquid.getBounds().getY());
		if (streamWidth > 15){
			streamWidth = 15;
		} else if(streamWidth < 3){
			streamWidth = 0;
		}

		// update all internal variables
	    this.transform = transform;
		this.liquid = fittedLiquid;
		this.streamPos = new Point2D.Double(streamX, streamY);
		if (volume > 2){
			this.streamWidth = streamWidth;
		} else {
			this.streamWidth = 0;
		}
		// Set the amount of percentage fluid to drop next update
		this.streamSpeed = (double)this.streamWidth/25.0;
	}
	
	public void breaking(){
		breaking = true;
	}
	
	public void setDraw(boolean draw){
		this.draw = draw;
	}
	
	public boolean getDraw(){
		return draw;
	}
	
	/**
	 * @ return ; the number of red the potion contains
	 */
	public int getRed() {
		return red;
	}

	/**
	 * @ return ; the number of green the potion contains
	 */
	public int getGreen() {
		return green;
	}

	/**
	 * @ return ; the number of blue the potion contains
	 */
	public int getBlue() {
		return blue;
	}
	
	/**
	 * @ return ; the name of the potion
	 */
	public String getName(){
		return name;
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(GeneralPath shape) {
		this.shape = shape;
	}
}
