package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Hand extends WorldObject
{
	private Rectangle2D.Double shape;
	private BufferedImage grabbedImage;
	private BufferedImage normalImage;
	private BufferedImage grabbedFrontImage;
	private boolean grabbed = false;

	public Hand(int xPos, int yPos, int number)
	{
		super(xPos, yPos);

		if (number == 1)
		{
			try
			{
				normalImage = ImageIO.read(new File("res/Hand_L.png"));
				grabbedImage = ImageIO.read(new File("res/Hand_L_grab.png"));
				grabbedFrontImage = ImageIO.read(new File(
						"res/Hand_L_grab_front.png"));
				image = normalImage;
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (number == 2)
		{
			try
			{
				normalImage = ImageIO.read(new File("res/Hand_R.png"));
				grabbedImage = ImageIO.read(new File("res/Hand_R_grab.png"));
				grabbedFrontImage = ImageIO.read(new File(
						"res/Hand_R_grab_front.png"));
				image = normalImage;
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		shape = new Rectangle2D.Double(xPos, yPos, 50, 50);

	}

	public void toggleGrabbed(int wiimoteID, boolean grabbed)
	{
		if (grabbed)
		{
			this.grabbed = true;
		}
		else
		{
			this.grabbed = false;
		}

	}

	public boolean isGrabbed()
	{
		return grabbed;
	}

	// draw normal hand
	public void draw(Graphics2D g2d)
	{
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.drawImage(image, transform, null);
	}

	// draw front grabbed image
	public void drawFront(Graphics2D g2d)
	{
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.drawImage(grabbedFrontImage, transform, null);
	}

	// draw grabbed image
	public void drawGrabbed(Graphics2D g2d)
	{
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.drawImage(grabbedImage, transform, null);
	}
}